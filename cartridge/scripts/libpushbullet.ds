/**
 * Demandware Script Wrapper for Pushbullet API calls (https://docs.pushbullet.com/#http)
 * @author Ranveer Raghuwanshi <ranveer.raghu@gmail.com>
 * @date 2015-04-10
 */
importPackage(dw.system);
importPackage(dw.svc);

function Pushbullet() {

	this.CLIENT_SECRET = Site.getCurrent().getCustomPreferenceValue("pbClientSecret");
	this.CLIENT_ID = Site.getCurrent().getCustomPreferenceValue("pbClientId");
	this.PB_AUTHORIZE_URL = Site.getCurrent().getCustomPreferenceValue("pbAuthorizeUrl");
	this.REDIRECT_URI = dw.web.URLUtils.https(Site.getCurrent().getCustomPreferenceValue("pbRedirectUrl"));
	this.RESPONSE_TYPE = "code";
	this.SCOPE = "everything";
	this.GRANT_TYPE = "authorization_code";
}

//TODO : Improve it
Pushbullet.prototype.getService = function (serviceType : String) : Service {

	var service : Service;
	switch (serviceType) {

	case "oauth":
		service = ServiceRegistry.get("service.http.pushbullet.oauth");
		break;
	default:
		service = ServiceRegistry.get("service.http.pushbullet");
		break;

	}
	return service;
};

//Get Oauth url for redirection
Pushbullet.prototype.getOauthUrl = function () : String {

	var paramObject : Object = {
		"client_id" : this.CLIENT_ID,
		"redirect_uri" : this.REDIRECT_URI,
		"response_type" : this.RESPONSE_TYPE,
		"scope" : this.SCOPE
	}

	var paramString = Object.keys(paramObject)
		.map(function (key) {
			return key + "=" + encodeURIComponent(paramObject[key]);
		})
		.join('&');

	var url : String = this.PB_AUTHORIZE_URL.concat('?', paramString);

	return url;

};

//API : https://docs.pushbullet.com/#oauth
Pushbullet.prototype.getAccessToken = function (code : String) : Result {

	var paramObject : Object = {
		"grant_type" : this.GRANT_TYPE,
		"client_id" : this.CLIENT_ID,
		"client_secret" : this.CLIENT_SECRET,
		"code" : code
	}
	var result : Result;
	service = service = this.getService("oauth"); ;
	result = this.makeServiceCall(service, paramObject);
	return result;

};

//API : https://docs.pushbullet.com/#devices
Pushbullet.prototype.retrieveDevices = function (accessToken : String) : Result {

	var service : Service;
	var result : Result;
	var paramObject : Object = {
		"accessToken" : accessToken
	}
	service = this.getService();
	result = this.makeServiceCall(service, paramObject, "devices");
	return result;
};

//API : https://docs.pushbullet.com/#contacts
Pushbullet.prototype.retrieveContacts = function (accessToken : String) : Result {

	var service : Service;
	var result : Result;
	var paramObject : Object = {
		"accessToken" : accessToken
	}
	service = this.getService();
	result = this.makeServiceCall(service, paramObject, "contacts");
	return result;
};

//API : https://docs.pushbullet.com/#pushes
Pushbullet.prototype.sendNotification = function (paramObject : Object) : Result {

	var result : Result;
	service = this.getService();
	result = this.makeServiceCall(service, paramObject, "pushes", "notification");
	return result;

};
Pushbullet.prototype.makeServiceCall = function (serviceObj : dw.svc.Service, paramObject : Object, method : String, action : String) : Result {

	var result : dw.svc.Result;

	//TODO : Need to improve below code logic
	if (method != undefined) {
		var endpointUrl : String = serviceObj.getURL();
		endpointUrl = endpointUrl + method;
		serviceObj.setURL(endpointUrl);
	}

	if (action != undefined) {
		result = serviceObj.call(paramObject, action);
	} else {
		result = serviceObj.call(paramObject);
	}

	return result;
};

//Extend Objects
Pushbullet.prototype.extend = function (target : Object, source) : Object {
	var _source : Object;

	if (!target)
		return source;

	for (var i = 1; i < arguments.length; i++) {
		_source = arguments[i];
		for (var prop in _source) {
			if ('object' === typeof _source[prop]) {
				target[prop] = this.extend(target[prop], _source[prop]);
			} else {
				target[prop] = _source[prop];
			}
		}
	}

	return target;
};
Pushbullet.prototype.handleJsonResponse = function (result : dw.svc.Result) : Object {

	var jsonString : String = "";
	if (result.ok) {
		jsonString = result.object;
	} else {
		jsonString = result.errorMessage;
	}
	var parsedJson : Object = JSON.parse(jsonString);

	return parsedJson;

}

module.exports.getInstance = function () {
	return new Pushbullet();
};
