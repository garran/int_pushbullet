# int_pushbullet
Pushbullet extension for Demandware.

Pushbullet bridges the gap between your phone, tablet, and computer, enabling them to work better together.
Using this integration customer will be able to directly send notification from storefront to their electronic devices. 

**TODO**

* Improve code logic in scripts
* Better Error Handling
* Remove certain hard-coded strings
* More polishing at front-end and back-end level
* Login with Pushbullet (Oauth). 
